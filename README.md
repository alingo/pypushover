# PyPushover
PyPushover is a Python library for sending [Pushover](https://pushover.net/) messages.

For API details, please see the [Official Pushover API Documentation](https://pushover.net/api).

## Features

### Supported Features
* [Specifying a custom message timestamp](https://pushover.net/api#timestamp)
* [Setting a message priority](https://pushover.net/api#priority), with `retry` and `expire` options for Emergency messages
* [Attaching a supplementary URL to messages](https://pushover.net/api#urls)
* [Playing alternate notification sounds for messages](https://pushover.net/api#sounds)
* [Attaching an image to messages](https://pushover.net/api#attachments)
* [Additional styling of messages as either HTML or monospace](https://pushover.net/api#html)
* [Callbacks or receipts for Emergency messages](https://pushover.net/api/receipts)


## Download Latest Release
You can download a Python Wheel from [GitLab](https://gitlab.com/alingo/pypushover/-/jobs/artifacts/master/download?job=wheel)

## Install Latest Release
1. Unzip the file downloaded from GitLab
2. ``python3 -m pip install --user dist/PyPushover-1.1-py3-none-any.whl``

## Example
```
# Import the library
import PyPushover

# Construct a simple message
message = PyPushover.PushoverMessage('MyAppToken', 'MyUserKey', 'This is the content of my message!')

# Send the message
message.send()

```
