import setuptools

with open("README.md") as fh:
    long_description = fh.read()

setuptools.setup(
    name="PyPushover",
    version="1.1",
    author="Alexander J. Lingo",
    author_email="A.Lingo@gmail.com",
    description="A library for sending Pushover messages",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[
          'requests',
    ],
    python_requires='>=3.7',
)
