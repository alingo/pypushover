#!/usr/bin/env python
"""
PyPushover is a Python library for sending Pushover messages.
"""
import datetime
import mimetypes
import os
import requests

# Image to use if the attached image is broken
BROKEN_IMAGE = os.path.join(os.path.dirname(__file__), 'broken-img-pixel.jpg')

# URLs of the Pushover API endpoints
PUSHOVER_MESSAGES_API = "https://api.pushover.net/1/messages.json"
PUSHOVER_LIMITS_API = "https://api.pushover.net/1/apps/limits.json?token={app_token}"
PUSHOVER_USER_VALIDATION_API = "https://api.pushover.net/1/users/validate.json?token={app_token}&user={user_key}"
PUSHOVER_SOUNDS_API = "https://api.pushover.net/1/sounds.json?token={app_token}"
PUSHOVER_RECEIPT_API = "https://api.pushover.net/1/receipts/{receipt_id}.json?token={app_token}"
PUSHOVER_EMERGENCY_MESSAGE_CANCEL_API = "https://api.pushover.net/1/receipts/{receipt_id}/cancel.json?token={app_token}"
PUSHOVER_EMERGENCY_MESSAGE_CANCEL_BY_TAG_API = "https://api.pushover.net/1/receipts/cancel_by_tag/{tag}.json?token={" \
                                               "app_token} "


def get_api_limits(app_token: str) -> dict:
    """
    Gets the current Pushover API limits.

    :param app_token: The 30-character Application Token of the Application.
    :return: A dictionary containing the returned API limits
    """
    # TODO: Return a custom limits object instead of API JSON.
    req = requests.get(PUSHOVER_LIMITS_API.format(app_token=app_token))
    return req.json()


def get_sounds(app_token: str) -> dict:
    """
     Gets the current Pushover supported sounds from the Pushover API.

    :param app_token: The 30-character Application Token of the Application.
    :return: A dictionary containing the returned Pushover sounds.
    """
    # TODO: Return a list of custom sounds objects instead of API JSON.
    req = requests.get(PUSHOVER_SOUNDS_API.format(app_token=app_token))
    return req.json()


def validate_user(app_token: str, user_key: str) -> dict:
    """
    Validates the user information with the Pushover API and returns user information.

    :param app_token: The 30-character Application Token of the Application.
    :param user_key:
    :return: A dictionary containing the returned Pushover information on the user.
    """
    # TODO: Return a custom user object instead of API JSON.
    req = requests.post(PUSHOVER_USER_VALIDATION_API.format(app_token=app_token, user_key=user_key))
    return req.json()


def get_user_devices(app_token: str, user_key: str) -> list:
    """
    Returns a list of Pushover client devices.
    :param app_token: The 30-character Application Token of the Application.
    :param user_key: Your user key as seen on the Pushover dashboard when logged in.
    :return: A list of Pushover client device names for the user.
    """
    user_info = validate_user(app_token, user_key)
    return user_info['devices']


def get_user_licensed_platforms(app_token: str, user_key: str) -> list:
    """
    Returns a list licensed Pushover platforms for the user.
    :param app_token: The 30-character Application Token of the Application.
    :param user_key: Your user key as seen on the Pushover dashboard when logged in.
    :return: A list of Pushover licensed platforms for the user.
    """
    user_info = validate_user(app_token, user_key)
    return user_info['licenses']


class PushoverMessage:
    """
    A Pushover message.
    """
    @classmethod
    def cancel_emergency_messages(cls, app_token: str, receipt_id='', tag='') -> dict:
        """
        Cancel emergency priority (2) messages sent via the Pushover API.

        :param app_token: The 30-character Application Token of the Application that send the message(s).
        :param receipt_id: The receipt ID of a previously sent emergency priority message to cancel retries for.
        :param tag: The tag used to send one or more emergency priority messages to cancel retries for.
        :return: The dictionary returned by the Pushover API.
        :raises PushoverError: If none of or both of tag and receipt ID are provided.
        """

        if (not receipt_id) and (not tag):
            raise PushoverError("You must provide either a receipt ID or tag to cancel messages!")

        if receipt_id and tag:
            raise PushoverError("You cannot provide both a receipt ID and a tag to cancel messages!")

        if receipt_id:
            req = requests.post(
                PUSHOVER_EMERGENCY_MESSAGE_CANCEL_API.format(app_token=app_token, receipt_id=receipt_id))

        else:
            req = requests.post(PUSHOVER_EMERGENCY_MESSAGE_CANCEL_BY_TAG_API.format(app_token=app_token, tag=tag))

        return req.json()

    @classmethod
    def get_receipt_status(cls, app_token: str, receipt_id: str):
        """
        Gets the message receipt status from the Pushover API.
        :param app_token: The 30-character Application Token of the Application sending the message.
        :param receipt_id: The receipt ID of the message to check the receipt status on.
        :return PushoverReceipt: Returns a PushoverReceipt instance with the receipt information.
        """
        req = requests.get(PUSHOVER_RECEIPT_API.format(app_token=app_token, receipt_id=receipt_id))
        return PushoverReceipt(req, receipt_id, app_token)

    def __init__(self, app_token, user_key, message, devices=(), title='', url='', url_title='', timestamp=None,
                 sound='', priority=0, retry=30, expire=3600, attachment=None, styling=None, callback_url='', tags=()):
        """
        Crafts a Pushover message.
        :param app_token: The 30-character Application Token of the Application sending the message.
        :param user_key: Your user key as seen on the Pushover dashboard when logged in.
        :param message: The message text to send.
        :param devices: An optional list of devices to send the message to. If unset, send to all user devices.
        :param title: The optional title for the message to show instead of the Application name.
        :param url: An optional clickable URL to send.
        :param url_title: An optional title for the URL.
        :param timestamp: An optional datetime instance to show for the message instead of the time the API receives it.
        :param sound: An optional sound to play for the message instead of the user's default notification sound.
        :param priority: An optional priority for the message. If unset, priority is normal (0).
        :param retry: The time between ack retries for emergency priority (2) messages.
        :param expire: The time before ack attempts expire for emergency priority (2) messages.
        :param callback_url: A callback URL the Pushover API servers will call when a emergency priority (2) message is
                             acknowledged by the user.
        :param tags: A list of tags for the message that can be used to cancel emergency priority (2) messages instead
                     of using each message's receipt ID.
        :param attachment: Path on disk to an optional image file to attach to the message.
        :param styling: An optional style for the message, either 'html' or 'monospace'.
        :raises PushoverError: If any message options conflict or are invalid.
        """
        # Initialize variables
        self.__app_token: str = ''
        self.__user_key: str = ''
        self.__message: str = ''
        self.__title: str = ''
        self.__devices: list = []
        self.__url: str = ''
        self.__url_title: str = ''
        self.__timestamp = None
        self.__sound: str = ''
        self.__priority: int = 0
        self.__retry: int = 0
        self.__expire: int = 0
        self.__attachment: str = ''
        self.__styling: str = ''
        self.__api_response = None
        self.__callback_url: str = ''
        self.__tags: list = []
        self.__response = None
        self.__receipt = None

        # Set variables
        self.set_app_token(app_token)
        self.set_user_key(user_key)
        self.set_message(message)
        self.set_devices(devices)
        self.set_title(title)
        self.set_url(url)
        self.set_url_title(url_title)
        self.set_timestamp(timestamp)
        self.set_sound(sound)
        self.set_priority(priority)
        self.set_retry(retry)
        self.set_expire(expire)
        self.set_attachment(attachment)
        self.set_styling(styling)
        self.set_callback_url(callback_url)
        self.set_tags(tags)
        self.__status = 'unsent'
        self.__api_response = None

    @property
    def app_token(self) -> str:
        """
        :return: Returns the Application Token used with the message.
        """
        return self.__app_token

    def set_app_token(self, app_token: str) -> None:
        """
        Sets the Pushover application token for this message.
        :param app_token: The Pushover application token to use for this message.
        """
        self.__app_token = app_token

    @property
    def user_key(self) -> str:
        """
        :return: The user key used for this message.
        """
        return self.__user_key

    def set_user_key(self, user_key: str) -> None:
        """
        Sets the Pushover user key for this message.
        :param user_key: The user key used for this message.
        """
        self.__user_key = user_key

    @property
    def message(self):
        """
        :return: The message text to send.
        """
        return self.__message

    def set_message(self, message: str) -> None:
        """
        Sets the Pushover message content for this message.
        :param message: The message text to send.
        ;raises PushoverError: If the message length exceeds the 1024 Unicode character API limit.
        """
        if len(message) > 1024:
            raise PushoverError("Message length cannot exceed 1024 Unicode characters!")
        self.__message = message

    @property
    def title(self):
        """
        :return: The optional title for the message to show instead of the Application name.
        """
        return self.__title

    def set_title(self, title: str) -> None:
        """
        Sets the Pushover message title for this message.
        :param title: The optional title for the message to show instead of the Application name.
        ;raises PushoverError: If the title length exceeds the 250 Unicode character API limit.
        """
        if len(title) > 250:
            raise PushoverError("Title length cannot exceed 250 Unicode characters!")
        self.__title = title

    @property
    def devices(self):
        """
        :return: An optional list of devices to send the message to. If unset, send to all user devices.
        """
        return self.__devices

    def set_devices(self, devices: list) -> None:
        """
        Sets the Pushover target devices for this message.
        :param devices: An optional list of devices to send the message to. If unset, send to all user devices.
        """
        # Make sure we have a list of strings
        if type(devices) == str or type(devices) == bytes:
            devices = [devices]
        self.__devices = devices

    @property
    def tags(self):
        """
        :return: A list of tags for the message that can be used to cancel emergency priority (2) messages instead of
                 using each message's receipt ID.
        """
        return self.__tags

    def set_tags(self, tags: list) -> None:
        """
        Sets the Pushover tags for this message.
        :param tags: A list of tags for the message that can be used to cancel emergency priority (2) messages instead
                     of using each message's receipt ID.
        """
        # Make sure we have a list of strings
        if type(tags) == str or type(tags) == bytes:
            tags = [tags]
        self.__tags = tags

    @property
    def url(self):
        """
        :return: An optional clickable URL to send.
        """
        return self.__url

    def set_url(self, url: str) -> None:
        """
        Sets the supplemental URL for this message.
        :param url: An optional clickable URL to send.
        :raises PushoverError: If the URL length exceeds the 512 character API limit.
        """
        if len(url) > 512:
            raise PushoverError("URL length cannot exceed 512 characters!")
        self.__url = url

    @property
    def callback_url(self):
        """
        :return: A callback URL the Pushover API servers will call when a emergency priority (2) message is
                 acknowledged by the user
        """
        return self.__callback_url

    def set_callback_url(self, callback_url: str) -> None:
        """
        Sets the callback URL for this message.
        :param callback_url: A callback URL the Pushover API servers will call when a emergency priority (2) message is
               acknowledged by the user
        """
        self.__callback_url = callback_url

    @property
    def url_title(self):
        """
        :return: The supplemental URL title set for this message.
        """
        return self.__url_title

    def set_url_title(self, url_title: str) -> None:
        """
        Sets the supplemental URL title for this message.
        :param url_title: he supplemental URL title for this message.
        :raises PushoverError: If the URL title length exceeds the 100 Unicode character API limit.
        """
        if len(url_title) > 100:
            raise PushoverError("URL title length cannot exceed 100 characters!")
        self.__url_title = url_title

    @property
    def timestamp(self):
        """
        :return: The custom datetime instance set to show as the message time for this message.
        """
        return self.__timestamp

    def set_timestamp(self, timestamp: datetime.datetime) -> None:
        """
        Sets the custom datetime instance as the message time for this message.
        :param timestamp: The custom datetime instance to set as the message time for this message.
        """
        self.__timestamp = timestamp

    @property
    def sound(self):
        """
        Returns the notification sound set for this message.
        :return: The notification sound set for this message.
        """
        return self.__sound

    def set_sound(self, sound: str) -> None:
        """
        Sets the notification sound for this message.
        :param sound: The notification sound for this message.
        """
        # TODO: Validate that the selected sound is an option from the Pushover API.
        self.__sound = sound

    @property
    def priority(self):
        """
        :return: Returns the priority value set for this message.
        """
        return self.__priority

    def set_priority(self, priority: int) -> None:
        """
        Sets the priority value for this message.
        :param priority:
        :return: The priority value for this message.
        :raises PushoverError: If the priority value is invalid.
        """
        if priority < -2 or priority > 2:
            raise PushoverError("Priority values can only be between -2 and 2!")
        self.__priority = priority

    @property
    def retry(self):
        """
        :return: The retry value set for this message.
        """
        return self.__retry

    def set_retry(self, retry: int) -> None:
        """
        Sets the retry value for this message.
        :param retry: The retry value for this message.
        :raises PushoverError: If the retry value is less than the 30 second minimum.
        """
        if retry < 30:
            raise PushoverError("Emergency message retry value must be at least 30 seconds!")
        self.__retry = retry

    @property
    def expire(self):
        """
        :return: The expiry value set for this message.
        """
        return self.__priority

    def set_expire(self, expire: int) -> None:
        """
        Sets the expiry value for this message.
        :param expire: The expiry value for this message.
        :raises PushoverError: If the expiry value is greater than the 10800 second maximum.
        """
        if expire > 10800:
            raise PushoverError("Emergency message expiry time must be at most 10800 seconds!")
        self.__expire = expire

    @property
    def styling(self):
        """
        :return: The styling option for this message.
        """
        return self.__styling

    def set_styling(self, styling: str) -> None:
        """
        Sets the styling for this message.
        :param styling: The styling option for this message.
        :raises PushoverError: If the styling option is invalid.
        """
        if styling not in ('monospace', 'html', '', None):
            raise PushoverError("Only 'html' and 'monospace' are valid message styling options!")
        self.__styling = styling

    @property
    def attachment(self):
        """
        :return: The path of the file to attach to this message.
        """
        return self.__attachment

    def set_attachment(self, attachment: str) -> None:
        """
        Sets the path of the file to attach to this message.
        :param attachment: The path of the file to attach to this message.
        """
        # TODO: Validate the path on disk exists and is readable.
        self.__attachment = attachment

    @property
    def response(self):
        """
        :return: A PushoverMessageResponse instance if the message has been sent.
        """
        return self.__response

    def __get_api_payload(self) -> dict:
        """
        Generates the Pushover API payload data.
        :return: The Pushover API payload data to attach to the POST request.
        """
        # Set the required fields
        payload = {'token': self.__app_token, 'user': self.__user_key, 'message': self.__message}

        # Set optional fields
        if self.__devices:
            payload['device'] = ",".join(self.__devices)
        if self.__title:
            payload['title'] = self.__title
        if self.__url:
            payload['url'] = self.__url
        if self.__url_title:
            payload['url_title'] = self.__url_title
        if self.__timestamp:
            payload['timestamp'] = str(int(datetime.datetime.timestamp(self.__timestamp)))
        if self.__sound:
            payload['sound'] = self.__sound
        if self.__priority:
            payload['priority'] = str(self.__priority)
        if self.__priority == 2:
            if self.__retry:
                payload['retry'] = str(self.__retry)
            if self.__expire:
                payload['expire'] = str(self.__expire)
        if self.__styling:
            if self.__styling == 'html':
                payload['html'] = 1
            elif self.__styling == 'monospace':
                payload['monospace'] = 1
            else:
                pass

        # Handle the emergency priority (2) fields
        if self.__priority == 2:
            if self.__callback_url:
                payload['callback'] = self.__callback_url
            if self.__tags:
                payload['tags'] = ",".join(self.__tags)

        # Return the generated data
        return payload

    def __get_attachment(self) -> dict:
        """
        Generates the Pushover API file attachment data.
        :return: The Pushover API file attachment data to attach to the POST request.
        """
        # Attach the file if given and send
        if self.__attachment:
            if not os.path.exists(self.__attachment):
                self.__attachment = BROKEN_IMAGE
            attachment_name = os.path.basename(self.__attachment)
            attachment_mimetype = mimetypes.guess_type(attachment_name)[0]
            files = {"attachment": (attachment_name, open(self.__attachment, "rb"), attachment_mimetype)}
            return files
        else:
            return {}

    def send(self) -> None:
        """
        Sends the Pushover Message.
        :raises PushoverError: If the message has already been sent, or if a sending error occurs.
        """
        # Raise an error if we try to re-send an already sent message.
        if self.__status == 'sent':
            raise PushoverError("Pushover message already sent!")

        # Get the API payload
        payload = self.__get_api_payload()

        # Get the file attachment and send the request
        if self.__attachment:
            files = self.__get_attachment()
            req = requests.post(PUSHOVER_MESSAGES_API, data=payload, files=files)
        else:
            req = requests.post(PUSHOVER_MESSAGES_API, data=payload)

        # Create a PushoverMessageResponse instance and attach it to the response property of the message
        response = PushoverMessageResponse(req, self.__app_token, self.__user_key, self.__priority)
        self.__response = response

        # Raise an error if the sending failed, otherwise mark the message as sent.
        if req.status_code != 200:
            raise PushoverError("Pushover returned HTTP " + str(req.status_code) + ": " + req.content.decode("utf-8"))
        else:
            self.__status = 'sent'


class PushoverMessageResponse:
    """
    A Pushover message response from the Pushover API.
    """

    def __init__(self, requests_response, app_token, user_key, priority):
        """
        Initialize a PushoverMessageResponse instance
        :param requests_response: The response from the message API call.
        :param app_token: The application token used to send the message.
        :param user_key: The user key used to send the message.
        :param priority: The priority of the send message.
        :returns: PushoverMessageResponse instance
        """
        api_response = requests_response.json()
        self.__app_token = app_token
        self.__user_key = user_key
        self.__http_status_code = requests_response.status_code
        self.__status = api_response['status']
        self.__priority = priority
        self.__request = api_response['request']
        if 'errors' in api_response:
            self.__errors = api_response['errors']
        else:
            self.__errors = []
        if 'receipt' in api_response:
            self.__receipt_id = api_response['receipt']
        else:
            self.__receipt_id = None
        if 'user' in api_response:
            self.__user = api_response['user']
        else:
            self.__user = None
        if 'token' in api_response:
            self.__token = api_response['token']
        else:
            self.__token = None

    def __str__(self):
        """
        :return: A string representing the PushoverMessageResponse instance.
        """
        return f"Pushover message { self.__request } response"

    def __repr__(self):
        """
        :return: A string representing the PushoverMessageResponse instance.
        """
        return self.__str__()

    @property
    def receipt(self):
        """
        :return: PushoverReceipt instance for this response if the message was emergency priority (2).
        """
        if self.__receipt_id:
            return PushoverMessage.get_receipt_status(self.__app_token, self.__receipt_id)
        else:
            return None

    @property
    def http_status_code(self):
        """
        :return: The HTTP status code of the Pushover API call.
        """
        return self.__http_status_code

    @property
    def status(self):
        """
        :return: The Pushover API request status code.
        """
        return self.__status

    @property
    def request(self):
        """
        :return: The Pushover API request ID.
        """
        return self.__request

    @property
    def errors(self):
        """
        :return: A list of errors given by the Pushover API.
        """
        return self.__errors

    @property
    def user(self):
        """
        :return: 'invalid' if the user key given was invalid.
        """
        return self.__user

    @property
    def receipt_id(self):
        """
        :return: The receipt ID if the message was emergency priority (2)
        """
        return self.__receipt_id

    @property
    def token(self):
        """
        :return: 'invalid' if the application token given was invalid.
        """
        return self.__token


class PushoverReceipt:
    """
    A Pushover receipt status report from the Pushover API
    """
    def __init__(self, requests_response, receipt_id, app_token):
        """
        Initialize a PushoverReceipt instance
        :param requests_response: The response from the receipt status API call.
        :param receipt_id: The ID of the receipt.
        :param app_token: The application token used to send the message.
        :returns: PushoverReceipt instance.
        """
        api_response = requests_response.json()
        self.__app_token = app_token
        self.__http_status_code = requests_response.status_code
        self.__status = int(api_response['status'])
        self.__acknowledged = bool(api_response['acknowledged'])
        self.__acknowledged_at = datetime.datetime.fromtimestamp(int(api_response['acknowledged_at']))
        self.__acknowledged_by = api_response['acknowledged_by']
        self.__acknowledged_by_device = api_response['acknowledged_by_device']
        self.__last_delivered_at = datetime.datetime.fromtimestamp(int(api_response['last_delivered_at']))
        self.__expired = bool(api_response['expired'])
        self.__expires_at = datetime.datetime.fromtimestamp(int(api_response['expires_at']))
        self.__called_back = bool(api_response['called_back'])
        self.__called_back_at = datetime.datetime.fromtimestamp(int(api_response['called_back_at']))
        self.__receipt_id = receipt_id

    def __str__(self):
        """
        :return: A string representing the PushoverReceipt instance.
        """
        return f"Pushover receipt { self.__receipt_id } status"

    def __repr__(self):
        """
        :return: A string representing the PushoverReceipt instance.
        """
        return self.__str__()

    def cancel_retries(self):
        """
        Cancel the remaining acknowledgement retires for this receipt.
        :return:
        """
        if (not self.__expired) and (not self.__acknowledged):
            PushoverMessage.cancel_emergency_messages(self.__app_token, self.__receipt_id)

    @property
    def http_status_code(self):
        """
        :return: The HTTP status code of the Pushover API call.
        """
        return self.__http_status_code

    @property
    def status(self):
        """
        :return: The Pushover API status code.
        """
        return self.__status

    @property
    def acknowledged(self):
        """
        :return: If the emergency priority (2) message has been acknowledged.
        """
        return self.__acknowledged

    @property
    def acknowledged_at(self):
        """
        :return: The datetime instance of when the emergency priority (2) message was acknowledged.
        """
        return self.__acknowledged_at

    @property
    def acknowledged_by(self):
        """
        :return: The user that acknowledged the emergency priority (2) message.
        """
        return self.__acknowledged_by

    @property
    def acknowledged_by_device(self):
        """
        :return: The device on which the user acknowledged the emergency priority (2) message.
        """
        return self.__acknowledged_by_device

    @property
    def last_delivered_at(self):
        """
        :return: The datetime instance of the last time the emergency priority (2) message was delivered.
        """
        return self.__last_delivered_at

    @property
    def expired(self):
        """
        :return: Has the emergency priority (2) message expired or been canceled.
        """
        return self.__expired

    @property
    def expires_at(self):
        """
        :return: The datetime instance of when the emergency priority (2) message will expire.
        """
        return self.__expires_at

    @property
    def called_back(self):
        """
        :return: Have the Pushover API servers hit the callback URL of the emergency priority (2) message
        """
        return self.__called_back

    @property
    def called_back_at(self):
        """
        :return: The datetime instance of when the Pushover API servers hit the callback URL of the emergency priority
                 (2) message.
        """
        return self.__called_back_at

    @property
    def receipt_id(self):
        """
        :return: The ID of this receipt.
        """
        return self.__receipt_id


class PushoverError(Exception):
    """
    A Pushover error message.
    """
    def __init__(self, msg):
        """
        Initialize a PushoverError instance.
        :param msg: The error message for this error.
        :returns: A PushoverError instance.
        """
        self.msg = msg

    def __str__(self):
        """
        :return: A string representing the PushoverError instance.
        """
        return repr(self.msg)
